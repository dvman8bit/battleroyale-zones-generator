<?php
require_once 'DB.php';
function get($key) {
    //for test
    $params = ['zones'=>[
        1=>['scale'=>1.5,'start_time'=>120,'move_time'=>90,'damage'=>10],
        2=>['scale'=>2,'start_time'=>90,'move_time'=>60,'damage'=>15],
        3=>['scale'=>2,'start_time'=>90,'move_time'=>60,'damage'=>20],
        4=>['scale'=>2,'start_time'=>90,'move_time'=>60,'damage'=>30],
        5=>['scale'=>4,'start_time'=>60,'move_time'=>30,'damage'=>40],
        ]];
    if(isset($key,$params)) {
        return $params[$key];
    }
    return null;
}
class Zone {

    public $img;
    public $center = 251;
    public $color = 50;

    public function generateNewZone($parentZone,$newZoneParams) {
        //hardcode some params
        $scale = $newZoneParams['scale'];
        $start_time = $newZoneParams['start_time'];
        $move_time = $newZoneParams['move_time'];
        $damage = $newZoneParams['damage'];

        //we need to generate circle which will fit in parent

        //calculate new center pos;
        $maxCenterRadius = $parentZone['radius']/2-($parentZone['radius']/$newZoneParams['scale']/2);
        $newCenter = rand(0,$maxCenterRadius);

        $newRad = $parentZone['radius']/$newZoneParams['scale'];

        $angle = rand(1,89);


        $xDirection = [-1,1][rand(0,1)];
        $yDirection = [-1,1][rand(0,1)];
        $newX = $parentZone['center'][0]+$newCenter*cos($angle)*$xDirection;
        $newY = $parentZone['center'][2]+$newCenter*sin($angle)*$yDirection;

        $this->drawDebugCircle($newRad,[$newX,0,$newY]);

        return ['scale'=>$scale,'damage'=>$damage, 'move_time'=>$move_time,'start_time'=>$start_time,'center'=>[$newX,0,$newY],'radius'=>$newRad];
    }

    public function drawDebugCircle($raduis,$center)
    {
        // создание пустого изображения
        if( ! $this->img) {
            $this->img = imagecreatetruecolor(1502, 1502);
            imagecolorallocate($this->img, 0, 0, 0);
        }
        $this->color+=30;
        $col_ellipse = imagecolorallocate($this->img, 255, 255, 255);

            imagefilledellipse($this->img, (int)$center[0]+$this->center, (int)$center[2]+$this->center, $raduis, $raduis, $col_ellipse);
            $col_ellipse = imagecolorallocate($this->img, 0, 0, 0);
            imagefilledellipse($this->img, (int)$center[0]+$this->center, (int)$center[2]+$this->center, $raduis-2, $raduis-2, $col_ellipse);
           //save
    }
    public function saveDebugImage()
    {
        unlink('/var/www/it-man.website/zone.gif');
        imagegif($this->img, '/var/www/it-man.website/zone.gif');
    }
}

if(!isset($_REQUEST['room_id'])) {
    die('[]');
}

$zones = getZoneById($_REQUEST['room_id']);
if(empty($zones)) {
    $zones = [];
    $zones[] = ['center'    => [500, 0, 500], 'scale' => 1, 'start_time' => 0,
                'move_time' => 60, 'damage' => 5, 'radius' => 1500];

    $zoneGenerator = new Zone();
    $zoneGenerator->drawDebugCircle($zones[0]['radius'], $zones[0]['center']);
    for ($i = 0; $i < count(get('zones')); $i++) {

        $params = get('zones')[$i + 1];

        $zones[] = $zoneGenerator->generateNewZone($zones[$i], $params);

    }
    $zoneGenerator->saveDebugImage();
    createZoneById($_REQUEST['room_id'],json_encode($zones));
}
echo json_encode($zones);
?>
<?php if( ! isset($_REQUEST['game'])):?>
<img src="https://it-man.website/zone.gif?nocache=<?php echo rand(99999,9999999)?>"/>
<?php endif;?>